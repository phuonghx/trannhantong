<?php
/*
Template Name: One Page Template
*/
get_header(); ?>
<section id="primary" class="content-full-width"><?php
	if( have_posts() ) :
		while( have_posts() ):
			the_post();
			get_template_part( 'template-parts/content', 'page' );

			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		endwhile;
	endif;?>
</section><?php
get_footer();?>