<!DOCTYPE html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php kriya_viewport(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php wp_head(); ?>
</head><?php
$bg = kriya_option('pageoptions','comingsoon-bg');
$opacity = kriya_opts_get('comingsoon-bg-opacity', '1');
$position = kriya_opts_get('comingsoon-bg-position', 'center center');
$repeat = kriya_opts_get('comingsoon-bg-repeat', 'no-repeat');
$color = kriya_option('pageoptions','comingsoon-bg-color');
$showcolor = kriya_option('pageoptions','show-comingsoon-bg-color');

$estyle = kriya_option('pageoptions','comingsoon-bg-style');

$color = !empty($color) ? kriya_hex2rgb($color) : array('f', 'f', 'f');
$style = !empty($bg) ? "background:url($bg) $position $repeat;" : '';
$style .= (!empty($color) && isset($showcolor) ) ? "background-color:rgba(  $color[0],  $color[1],  $color[2], {$opacity});" : '';
$style .= !empty($estyle) ? $estyle : ''; ?>

<body <?php body_class('type7 under-construction'); ?> style="<?php echo esc_attr($style); ?>">

<div class="wrapper"><?php
	$pageid = kriya_option('pageoptions','comingsoon-pageid');
	if( is_null($pageid) ) {
		echo DTCoreShortcodesDefination::dtShortcodeHelper ( stripslashes( '[dt_sc_down_count/]' ) );
	} else {
		$page = get_post( $pageid, ARRAY_A );
		echo DTCoreShortcodesDefination::dtShortcodeHelper ( stripslashes($page['post_content']) );
	}?>
</div>
</body>
<?php wp_footer(); ?>
</html>