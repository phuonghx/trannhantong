<?php 
/*
Template Name: Blog Template
*/

get_header();
	$tpl_default_settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
	$tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();

	$page_layout  = array_key_exists( "layout", $tpl_default_settings ) ? $tpl_default_settings['layout'] : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar = false;
	$sidebar_class = "";
	
	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;
		
		case 'with-both-sidebar':
			$page_layout = "page-with-sidebar with-both-sidebar";
			$show_sidebar = $show_left_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar('left');?>
			</section><?php
		endif;
	endif;?>

	<section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php
		if( have_posts() ) :

			while( have_posts() ):

				the_post();

				get_template_part( 'template-parts/content', 'page' );

				echo '<div style="height:32px" class="vc_empty_space"><span class="vc_empty_space_inner"></span></div>';

			endwhile;
		endif;?>

		<!-- Blog Template --><?php

			$post_layout = isset( $tpl_default_settings['blog-post-layout'] ) ? $tpl_default_settings['blog-post-layout'] : "one-half";
			$post_per_page = isset($tpl_default_settings['blog-post-per-page']) ? $tpl_default_settings['blog-post-per-page'] : -1;
			$categories = isset($tpl_default_settings['blog-post-cats']) ? array_filter($tpl_default_settings['blog-post-cats']) : NULL;

			$show_date_meta = isset( $tpl_default_settings['show-date-info'] ) ? "" : "hidden";
			$show_comment_meta = isset( $tpl_default_settings['show-comment-info'] ) ? "comments" : "hidden";
			$show_author_meta = isset( $tpl_default_settings['show-author-info'] ) ? "" : "hidden";
			$show_category_meta = isset( $tpl_default_settings['show-category-info'] ) ? "" : "hidden";
			$show_tag_meta = isset( $tpl_default_settings['show-tag-info'] ) ? "" : "hidden";
			$show_post_format = isset( $tpl_default_settings['show-postformat-info'] )? "" : "hidden";

			$article_class = ( $show_date_meta == 'hidden' ) ? 'blog-entry entry-date-left entry-date-hidden': 'blog-entry entry-date-left';
			$article_class = array_key_exists('enable-border-style',$tpl_default_settings) ? $article_class.' outer-frame-border': $article_class;
			
			$container_class = $c_class = "";

			switch($post_layout):
			
				default:
				case 'one-column':
					$post_class = $show_sidebar ? "column dt-sc-one-column with-sidebar blog-fullwidth" : "column dt-sc-one-column blog-fullwidth";
					$columns = 1;
				break;
				
				case 'one-half-column':
					$post_class = $show_sidebar ? "column dt-sc-one-half with-sidebar" : "column dt-sc-one-half";
					$columns = 2;
					$container_class = "apply-isotope";
					$c_class = ".dt-sc-one-half";
				break;

				case 'one-third-column':
					$post_class = $show_sidebar ? "column dt-sc-one-third with-sidebar" : "column dt-sc-one-third";
					$columns = 3;
					$container_class = "apply-isotope";
					$c_class = ".dt-sc-one-third";
				break;
			endswitch;

			if ( get_query_var('paged') ) {
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) { 
				$paged = get_query_var('page');
			} else { 
				$paged = 1;
			}

		if ( empty( $categories ) ):
			$args = array( 'paged'=>$paged, 'posts_per_page'=>$post_per_page, 'post_type'=> 'post' );
		else:
			$exclude_cats = array_unique( $categories );
			$args = array( 'paged'=>$paged, 'posts_per_page'=>$post_per_page, 'category__not_in'=>$exclude_cats, 'post_type'=>'post' );
		endif;

		$the_query = new WP_Query($args);

		if( $the_query->have_posts() ):

			echo "<div data-column='".$c_class."' class='tpl-blog-holder ".esc_attr( $container_class )."'>";

			while( $the_query->have_posts() ):

				$the_query->the_post();
				
				$format = get_post_format(  get_the_id() );
				$format_link = 	get_post_format_link( $format );
				$link = get_permalink( get_the_id() );
				$link = rawurlencode( $link );
				
				$post_meta = get_post_meta(get_the_id() ,'_dt_post_settings',TRUE);
				$post_meta = is_array($post_meta) ? $post_meta : array();

				$custom_class = "";?>
				<div class="<?php echo esc_attr($post_class);?>">
					<article id="post-<?php the_ID();?>" <?php post_class($article_class);?>>
						<!-- Featured Image -->
                        <?php if( $format == "image" || empty($format) ) :
                                if( has_post_thumbnail() ) :?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full");?></a>
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "gallery" ) :
                                if( array_key_exists("items", $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                    echo '	<ul class="entry-gallery-post-slider">';
                                                foreach ( $post_meta['items'] as $item ) {
                                                    echo "<li><img src='". esc_url($item)."'/></li>";
                                                }
                                    echo '	</ul>';
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full");?></a>
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "video" ) :
                                if( array_key_exists('oembed-url', $post_meta) || array_key_exists('self-hosted-url', $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                    echo'	<div class="dt-video-wrap">';
                                                if( array_key_exists('oembed-url', $post_meta) ) :
                                                    echo wp_oembed_get($post_meta['oembed-url']);
                                                elseif( array_key_exists('self-hosted-url', $post_meta) ) :
                                                    echo wp_video_shortcode( array('src' => $post_meta['self-hosted-url']) );
                                                endif;
                                    echo '	</div>';
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full");?></a>
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            elseif( $format === "audio" ) :
                                if( array_key_exists('oembed-url', $post_meta) || array_key_exists('self-hosted-url', $post_meta) ) :
                                    echo '<div class="entry-thumb">';
                                            if( array_key_exists('oembed-url', $post_meta) ) :
                                                echo wp_oembed_get($post_meta['oembed-url']);
                                            elseif( array_key_exists('self-hosted-url', $post_meta) ) :
                                                $custom_class = "self-hosted-audio";
                                                echo wp_audio_shortcode( array('src' => $post_meta['self-hosted-url']) );
                                            endif;
                                    echo '</div>';
                                elseif( has_post_thumbnail() ):?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full");?></a>
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            else:
                                if( has_post_thumbnail() ) :?>
                                    <div class="entry-thumb">
                                        <a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full");?></a>
                                    </div><?php
                                else:
                                    $custom_class = "has-no-post-thumbnail";
                                endif;
                            endif;?>
						<!-- Featured Image -->

						<div class="entry-details">

                            <div class="entry-date <?php echo esc_attr($show_date_meta);?>">                            
                                <!-- date -->
                                 <span><?php echo get_the_date('d');?></span>
                                    <?php echo get_the_date('M');?>
                                <!-- date -->
                            </div><!-- .entry-date -->

                            <?php $tclass = ( ($show_comment_meta == "hidden") && ($show_author_meta == "hidden" ) && ($show_tag_meta == "hidden" ) && ($show_category_meta == "hidden" ) ) ? "hidden" : ""; ?>
                            <div class="entry-meta-data <?php echo esc_attr($tclass);?>">

                            	<!-- Author, Comment, Category & Tag -->
                                <p class="author <?php echo esc_attr( $show_author_meta );?>">
                                	<?php _e('By','kriya'); ?>
                                	<a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" title="<?php esc_attr_e('View all posts by ', 'kriya'); echo get_the_author();?>"><?php echo get_the_author();?></a>
                                </p>

                                <!-- comment -->
                                <p class="<?php echo esc_attr($show_comment_meta);?>"><?php
                                	comments_popup_link( __('<i class="pe-icon pe-chat"> </i> 0 Comment','kriya'),
                                		__('<i class="pe-icon pe-chat"> </i> 1 Comment','kriya'),
                                		__('<i class="pe-icon pe-chat"> </i> % Comments','kriya'),
                                		'',
                                		__('<i class="pe-icon pe-chat"> </i> No Comments','kriya'));?>
                                </p><!-- comment -->

                                <p class="<?php echo esc_attr( $show_category_meta );?> category"><?php _e('In','kriya'); echo ' '; the_category(', '); ?></p>

                                <?php the_tags("<p class='tags {$show_tag_meta}'>",', ',"</p>");?>
                                <!-- Author, Comment, Category & Tag -->
                            </div>

                            <div class="entry-title">
								<h4><a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'), the_title_attribute('echo=0'));?>"><?php the_title(); ?></a></h4>
                            </div>

                            <?php if( array_key_exists('blog-post-excerpt',$tpl_default_settings) && array_key_exists('blog-post-excerpt-length',$tpl_default_settings) ):?>
                            	<div class="entry-body">
                            		<?php echo kriya_excerpt($tpl_default_settings['blog-post-excerpt-length']);?>
                            	</div><?php
                            endif;

                            if( array_key_exists('enable-blog-readmore',$tpl_default_settings) ) :?>
                            	<!-- Read More Button -->
                            	<div class="vc_btn3-container vc_btn3-inline">
                            		<a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','kriya'), the_title_attribute('echo=0'));?>" class="vc_general vc_btn3 vc_btn3-size-xs vc_btn3-shape-square vc_btn3-style-classic vc_btn3-color-skincolor"><?php
                            			_e('Continue Reading...','kriya');
                            		?></a>
                            	</div><!-- Read More Button --><?php
                            endif; ?>

							<div class="entry-format <?php echo esc_attr($show_post_format);?>">
								<a class="ico-format" href="<?php echo esc_url(get_post_format_link( $format ));?>"></a>
                            </div>						
						</div>
					</article>
				</div><?php
			endwhile;
			echo "</div>";

			echo '<div class="pagination blog-pagination">';
			echo kriya_pagination( $the_query );
			echo '</div>';
		else:
			get_template_part( 'template-parts/content', 'none' );
		endif;?><!-- Blog Template -->
	</section><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar('right');?>
			</section><?php
		endif;
	endif;
get_footer(); ?>