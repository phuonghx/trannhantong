<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.','kriya'); ?></p>
<?php  return;
	endif;?>
    
    <h3><?php comments_number(esc_html__('No Comments','kriya'), esc_html__('Comment ( 1 )','kriya'), esc_html__('Comments ( % )','kriya') );?></h3>
    <?php if ( have_comments() ) : ?>
    
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
                    <div class="navigation">
                        <div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments','kriya'  ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments','kriya') ); ?></div>
                    </div> <!-- .navigation -->
        <?php endif; // check for comment navigation ?>
        
        <ul class="commentlist">
     		<?php wp_list_comments( array( 'callback' => 'kriya_comment_style' ) ); ?>
        </ul>
    
    <?php else: ?>
		<?php if ( ! comments_open() ) : ?>
            <p class="nocomments"><?php esc_html_e( 'Comments are closed.','kriya'); ?></p>
        <?php endif;?>    
    <?php endif; ?>
	
    <!-- Comment Form -->
    <?php if ('open' == $post->comment_status) :
			$comment_args = array( 
                'comment_notes_before' => '',
                'comment_notes_after' => '',
                'fields' => array(
                    'author' => '<p class="column dt-sc-one-third comment-form-author first">' . '<label for="author">' . __( 'Name', 'kriya' ) .'</label> ' .
                    '<input id="author" name="author" type="text" value="" size="30" maxlength="245"/></p>',
                    'email'  => '<p class="column dt-sc-one-third comment-form-email"><label for="email">' . __( 'Email', 'kriya' ) .'</label> ' .
                    '<input id="email" name="email" type="email" value="" size="30" maxlength="100" aria-describedby="email-notes"/></p>',
                    'url'    => '<p class="column dt-sc-one-third comment-form-url"><label for="url">' . __( 'Website', 'kriya' ) . '</label> ' .
                    '<input id="url" name="url" type="url" value="" size="30" maxlength="200" /></p>',
            ));

            comment_form( $comment_args );?>
	<?php endif; ?>