<?php 
/*
Template Name: Portfolio Template
*/
get_header();

	$tpl_default_settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
	$tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();

	$page_layout  = array_key_exists( "layout", $tpl_default_settings ) ? $tpl_default_settings['layout'] : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar = false;
	$sidebar_class = "";
	
	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;
		
		case 'with-both-sidebar':
			$page_layout = "page-with-sidebar with-both-sidebar";
			$show_sidebar = $show_left_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar('left');?>
			</section><?php
		endif;
	endif;?>

	<section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php

		if( have_posts() ) :

			while( have_posts() ):

				the_post();

				get_template_part( 'template-parts/content', 'page' );

				echo '<div style="height:32px" class="vc_empty_space"><span class="vc_empty_space_inner"></span></div>';
			endwhile;
		endif;?>

		<!-- Portfolio Template -->
		<?php $post_layout = isset( $tpl_default_settings['portfolio-post-layout'] ) ? $tpl_default_settings['portfolio-post-layout'] : "one-half-column";
			$allow_space =  isset( $tpl_default_settings['portfolio-grid-space'] ) ? "with-space" : "no-space";
			$space_size = isset( $tpl_default_settings['portfolio-grid-space-size'] ) && ( $allow_space == 'with-space') ? 'data-gutter ='.$tpl_default_settings['portfolio-grid-space-size'] : "";

			$post_per_page = $tpl_default_settings['portfolio-post-per-page'];
			$categories = isset($tpl_default_settings['portfolio-categories']) ? array_filter($tpl_default_settings['portfolio-categories']) : array();			
			
			# Post Class
			$post_class = $c_class = '';
			switch( $post_layout ):
				case 'one-fourth-column':
					$post_class = $show_sidebar ? " column dt-sc-one-fourth with-sidebar" : " column dt-sc-one-fourth";
					$columns = 4;
					$c_class = ".dt-sc-one-fourth";
				break;

				case 'one-third-column':
					$post_class = $show_sidebar ? " column dt-sc-one-third with-sidebar" : " column dt-sc-one-third";
					$columns = 3;
					$c_class = ".dt-sc-one-third";
				break;
				
				default:
				case 'one-half-column':
					$post_class = $show_sidebar ? " column dt-sc-one-half with-sidebar" : " column dt-sc-one-half";
					$columns = 2;
					$c_class = ".dt-sc-one-half";
				break;
			endswitch;

			#Pagination
			$paged = 1;
			if ( get_query_var('paged') ) { 
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			}

			#Query arg
			$args = array();
			if( empty($categories) ):
				$args = array( 'paged' => $paged ,'posts_per_page' => $post_per_page,'post_type' => 'dt_portfolios');
			else:
				$args = array(
					'paged' => $paged,
					'posts_per_page' => $post_per_page,
					'post_type' => 'dt_portfolios',
					'orderby' => 'ID',
					'order' => 'ASC',
					'tax_query' => array( 
						array(
							'taxonomy' => 'dt_portfolios_categories',
							'field' => 'id',
							'operator' => 'IN',
							'terms' => $categories
						)
					)
				);
			endif;
			#Query arg	
					
			/* Filter Option */
			if(empty($categories)):
				$categories = get_categories('taxonomy=dt_portfolios_categories&hide_empty=1');
			else:
				$c = array('taxonomy'=>'dt_portfolios_categories','hide_empty'=>1,'include'=>$categories);
				$categories = get_categories($c);
			endif;
			
			if( (sizeof($categories) > 1) && (array_key_exists("filter",$tpl_default_settings)) ) :
				$post_class .= " all-sort";?>
                <div class="dt-sc-sorting-container dt-sc-portfolio-sorting">
                	<a href="#" class="active-sort" title="" data-filter=".all-sort"><?php esc_html_e('All','kriya');?></a>
                    	<?php foreach( $categories as $category ):?>
                        		<a href='#' data-filter=".<?php echo esc_attr($category->category_nicename);?>-sort">
                                	<?php echo esc_html($category->cat_name);?>
                                </a>
                        <?php endforeach;?>
                 </div><?php
			endif; 
			/* Filter Option */

			$the_query = new WP_Query($args);
			if( $the_query->have_posts() ):
				$i = 1;?>
				<div data-column="<?php echo $c_class; ?>" class="dt-sc-portfolio-container <?php echo esc_attr($allow_space);?>" <?php echo $space_size;?> ><?php
					while( $the_query->have_posts() ):
						$the_query->the_post();
						$the_id = get_the_ID();
						$title = get_the_title( $the_id );

						$temp_class = $allow_space.' ';
						if($i == 1) $temp_class .= $post_class." first"; else $temp_class .= $post_class;
						if($i == $columns) $i = 1; else $i = $i + 1;
						
						if( array_key_exists("filter",$tpl_default_settings) ):
							$item_categories = get_the_terms( $the_id, 'dt_portfolios_categories' );
							if(is_object($item_categories) || is_array($item_categories)):
								foreach ($item_categories as $category):
									$temp_class .=" ".$category->slug.'-sort ';
								endforeach;
							endif;
						endif;

						#setting up images
							$portfolio_item_meta = get_post_meta($the_id,'_portfolio_settings',TRUE);
							$portfolio_item_meta = is_array($portfolio_item_meta) ? $portfolio_item_meta  : array();
							$items = false;
							
							if( array_key_exists('items_name', $portfolio_item_meta) ) {
								
								$items = true;
								
								$item =  $portfolio_item_meta['items_name'][0];
								$popup = $portfolio_item_meta['items'][0];
								
								if( "video" === $item ) {
									$x = array_diff( $portfolio_item_meta['items_name'] , array("video") );
									if( !empty($x) ) {
										$image = $portfolio_item_meta['items'][key($x)];
		                        	} else {
		                        		$image = '//placehold.it/1170X902.jpg&text=Video';
		                        	}								
								} else {
									if( sizeof($portfolio_item_meta['items']) > 1 ){
										$popup = $portfolio_item_meta['items'][1];
									}
									
									$image = $portfolio_item_meta['items'][0];
								}
							}
							
							if( has_post_thumbnail() ) {
								
								$image = wp_get_attachment_image_src(get_post_thumbnail_id( $the_id ), 'full', false);
								$image = $popup = $image[0];
								
								if( !$items ){
									$popup = $image;
								}							
							}elseif( $items ) {
								$image = $image;
								$popup = $popup;
							}else{
								$image = $popup = '//place-hold.it/1170X902.jpg&text='.$title;
							}
						#setting up images end
							
						if( array_key_exists("portfolio-masonry",$tpl_default_settings) ) :
							$portfolio_settings = get_post_meta ( $the_id, '_portfolio_settings', TRUE );
							$portfolio_settings = is_array ( $portfolio_settings ) ? $portfolio_settings : array();
							$masonry = array_key_exists("masonry-size",$portfolio_settings) ? $portfolio_settings['masonry-size'] : '';
							$temp_class .= ' '.$masonry;
						endif;?>
						<div id="dt_portfolios-<?php echo esc_attr($the_id);?>" class="<?php echo esc_attr( trim($temp_class));?>">
                            <div class="portfolio">
							<figure>
								<img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
								<div class="image-overlay">
									<div class="portfolio-detail">
										<?php echo kriya_like_love(); ?>                                                      
										<div class="dt-portfolio-meta">
											<h5><a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('Permalink to %s','kriya'), the_title_attribute('echo=0'));?>"><?php
												the_title();?></a></h5><?php
											$subtitle = get_post_meta ( $the_id, '_portfolio_subtitle',true);
											if( !empty( $subtitle) ){
												echo '<p>'.esc_html($subtitle).'</p>';
											}?>
										</div>
									</div>
								</div>
							</figure>
						</div>
						</div><?php
					endwhile;?>
				</div>
				<div class="pagination blog-pagination">
				<?php echo kriya_pagination( $the_query ); ?>
				</div><?php
			else:
				get_template_part( 'template-parts/content', 'none' );
			endif;?>
		<!-- Portfolio Template -->
	</section><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar('right');?>
			</section><?php
		endif;
	endif;
get_footer(); ?>		